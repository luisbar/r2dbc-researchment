package com.luisbar.r2dbc.researchment.services;

import com.luisbar.r2dbc.researchment.dtos.DepositDto;
import com.luisbar.r2dbc.researchment.entities.MoneyDepositEvent;
import com.luisbar.r2dbc.researchment.repositories.AccountRepository;
import com.luisbar.r2dbc.researchment.repositories.MoneyDepositEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private MoneyDepositEventRepository moneyDepositEventRepository;

    public Mono deposit(DepositDto request){
        return this.accountRepository.findById(request.getAccount())
        .doOnNext(account ->  account.setBalance(account.getBalance() + request.getAmount()))
        .flatMap(this.accountRepository::save)
        .thenReturn(toEvent(request))
        .flatMap(this.moneyDepositEventRepository::save);
    }
    
    private MoneyDepositEvent toEvent(DepositDto request){
        return MoneyDepositEvent.create(
            null,
            request.getAccount(),
            request.getAmount()
        );
    }
}

