package com.luisbar.r2dbc.researchment.services;

import com.luisbar.r2dbc.researchment.dtos.DepositDto;
import com.luisbar.r2dbc.researchment.helpers.Requester;
import com.luisbar.r2dbc.researchment.models.GenericError;
import com.luisbar.r2dbc.researchment.models.SaveDepositResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class LedgerService {
    
    @Autowired
    private Requester requester;
    
    public Mono saveDeposit(DepositDto depositDto) {
        return requester
        .getWebClient()
        .get()
        .uri("/wallet/" + depositDto.getAccount())
        .exchange()
        .flatMap(response -> {
            if (response.statusCode().is5xxServerError() || response.statusCode().is4xxClientError())
                return Mono.error(new GenericError(response.statusCode().getReasonPhrase(), response.statusCode().value()));

            return response.bodyToMono(SaveDepositResponse.class);
        });
    }
}
