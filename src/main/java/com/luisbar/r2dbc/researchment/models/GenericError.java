package com.luisbar.r2dbc.researchment.models;

import lombok.Data;

@Data
public class GenericError extends Exception {
    
    private String message;
    private int code;

    public GenericError(String message, int code) {
        super(message);
        this.message = message;
        this.code = code;
    }
}
