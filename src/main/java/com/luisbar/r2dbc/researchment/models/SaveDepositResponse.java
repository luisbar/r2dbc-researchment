package com.luisbar.r2dbc.researchment.models;

import lombok.Data;

@Data
public class SaveDepositResponse {
    
    private String account;
    private String amount;
}
