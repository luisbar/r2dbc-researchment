package com.luisbar.r2dbc.researchment.helpers;

import lombok.Data;
import org.springframework.web.reactive.function.client.WebClient;

@Data
public class Requester {
    
    private final WebClient webClient;

    public Requester(String baseUrl) {
        this.webClient = WebClient
        .builder()
        .baseUrl(baseUrl)
        .build();
    }
}
