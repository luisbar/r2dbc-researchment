package com.luisbar.r2dbc.researchment.repositories;

import com.luisbar.r2dbc.researchment.entities.MoneyDepositEvent;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MoneyDepositEventRepository extends ReactiveCrudRepository<MoneyDepositEvent, Integer> {
}

