package com.luisbar.r2dbc.researchment.repositories;

import com.luisbar.r2dbc.researchment.entities.Account;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends ReactiveCrudRepository<Account, Integer> {
}
