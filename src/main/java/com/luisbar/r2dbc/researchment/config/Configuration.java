package com.luisbar.r2dbc.researchment.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@org.springframework.context.annotation.Configuration
@ConfigurationProperties("app")
@Data
public class Configuration {

    private String baseUrl;
}
