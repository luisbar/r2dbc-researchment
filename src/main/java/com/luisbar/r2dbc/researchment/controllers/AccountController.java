package com.luisbar.r2dbc.researchment.controllers;

import com.luisbar.r2dbc.researchment.dtos.DepositDto;
import com.luisbar.r2dbc.researchment.services.AccountService;
import com.luisbar.r2dbc.researchment.services.LedgerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.reactive.TransactionalOperator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@Slf4j
public class AccountController {
    
    @Autowired
    AccountService accountService;
    @Autowired
    LedgerService ledgerService;
    @Autowired
    private TransactionalOperator operator;
    
    @GetMapping("/account")
    public Mono<ResponseEntity> getAccount() throws Exception {
        DepositDto depositDto = new DepositDto();
        depositDto.setAccount(1);
        depositDto.setAmount(100);
        
        Mono result = (Mono) accountService
        .deposit(depositDto)
        .flatMap(moneyDepositEvent -> ledgerService.saveDeposit(depositDto))
        .as(saveDepositResponse -> operator.transactional((Mono) saveDepositResponse));
        
        return result;
    }
}
