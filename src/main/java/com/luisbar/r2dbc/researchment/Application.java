package com.luisbar.r2dbc.researchment;

import com.luisbar.r2dbc.researchment.config.Configuration;
import com.luisbar.r2dbc.researchment.helpers.Requester;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
    
    @Autowired
    private Configuration configuration;
    
    @Bean
    public Requester getRequester() {
      return new Requester(configuration.getBaseUrl());
    }
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
