The Reactive Relational Database Connectivity (R2DBC) project brings reactive programming APIs to relational databases.

# In a Nutshell

- Based on the Reactive Streams specification. R2DBC is founded on the Reactive Streams specification, which provides a fully-reactive non-blocking API.

- Works with relational databases. In contrast to the blocking nature of JDBC, R2DBC allows you to work with SQL databases using a reactive API.

- Supports scalable solutions. With Reactive Streams, R2DBC enables you to move from the classic “one thread per connection” model to a more powerful and scalable approach.

- Provides an open specification. R2DBC is an open specification and establishes a Service Provider Interface (SPI) for driver vendors to implement and clients to consume.

You can find more information [here](https://r2dbc.io)

# Requirements

For running this project you need the JDK 8 and Maven